# Arosaje - data-api

School project made by Anthony Diaz, Florent Castel & Paul Bouloc.

## Introduction

This is a mobile application designed for plant care and exchanging advice among individuals.

The project contains 5 parts:
- Data API ([View source](https://gitlab.com/arosaje-epsi/data-api))
- Auth API ([View source](https://gitlab.com/arosaje-epsi/auth-api))
- Mobile app ([View source](https://github.com/Gamper034/a_rosa_je))
- Back office ([View source](https://gitlab.com/arosaje-epsi/back-office))
- Database ([View source](https://gitlab.com/arosaje-epsi/database))

## Tech stack

- The two APIs are built with Node/Express.js and use Prisma as the ORM.
- The mobile application is developed using Flutter.
- The back-office is a web application developed with React/Next.js, with Tailwind and Shadcn as UI library.
- The database is SQLite.

## Getting started

To get the project up and running, and due to the technical constraints imposed by the initial brief, it is necessary to clone at least the Data API, the Auth API, and the Database within the same directory.

```
arosaje-project
├── data-api
├── auth-api
└── database
```

To initialize the APIs, you need to execute the following commands in each of the two directories:

```
npm install
npx prisma generate
```

Then, you'll need to add the following environment variables by creating two files in each of the two directories: `.env.development` and `.env.testing`.

### data-api

For the `.env.development` file:
```
PORT=2000
DATABASE_URL="file:../../database/db.sqlite"
AUTH_API_URL="http://localhost:1000/api"
COOKIE_SECRET_KEY="your_key"
JWT_SECRET_KEY="your_key"
```

For the `.env.testing` file:
```
PORT=2000
DATABASE_URL="file:../../database/db_testing.sqlite"
AUTH_API_URL="http://localhost:1000/api"
COOKIE_SECRET_KEY="your_key"
JWT_SECRET_KEY="your_key"
```

Make sure to replace "your_key" with the actual secret keys you intend to use.

### auth-api

For the `.env.development` file:
```
PORT=1000
DATABASE_URL="file:../../database/db.sqlite"
AUTH_API_URL="http://localhost:1000/api"
COOKIE_SECRET_KEY="your_key"
JWT_SECRET_KEY="your_key"
```

For the `.env.testing` file:
```
PORT=1000
DATABASE_URL="file:../../database/db_testing.sqlite"
AUTH_API_URL="http://localhost:1000/api"
COOKIE_SECRET_KEY="your_key"
JWT_SECRET_KEY="your_key"
```

Make sure to replace "your_key" with the actual secret keys you intend to use.

You can then launch both APIs with the command `npm run dev` to be executed in each of the directories.