FROM node:18
WORKDIR /usr/src/app
COPY package*.json ./
RUN rm -rf node_modules
RUN npm install
COPY . .
RUN npx prisma generate
EXPOSE 1000
CMD [ "npm", "run", "dev" ]