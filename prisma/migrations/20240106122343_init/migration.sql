/*
  Warnings:

  - Added the required column `adress` to the `Guard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `city` to the `Guard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `zipcode` to the `Guard` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Guard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "ownerId" TEXT NOT NULL,
    "startDate" DATETIME NOT NULL,
    "endDate" DATETIME NOT NULL,
    "adress" TEXT NOT NULL,
    "zipcode" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "guardianId" TEXT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "Guard_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Guard_guardianId_fkey" FOREIGN KEY ("guardianId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Guard" ("createdAt", "endDate", "guardianId", "id", "ownerId", "startDate") SELECT "createdAt", "endDate", "guardianId", "id", "ownerId", "startDate" FROM "Guard";
DROP TABLE "Guard";
ALTER TABLE "new_Guard" RENAME TO "Guard";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
