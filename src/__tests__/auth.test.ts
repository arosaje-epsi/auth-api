import supertest from "supertest"
import app from "../app"
import { PrismaClient } from "@prisma/client";
const bcrypt = require("bcrypt");
const prisma = new PrismaClient();

const jwt = require("jsonwebtoken");

import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});

const jwtSecretKey = process.env.JWT_SECRET_KEY;

describe('Auth', () =>{

  describe('Given user try to login', () => {

    let user;
    let pendingUser;
    let rejectedUser;
    const password = "password";

    beforeAll(async () => {

      const hashedPassword = await bcrypt.hash(password, 10);

      user = await prisma.user.create({
        data: {
          email: "john@doe1.com",
          password: hashedPassword,
          firstname: "John",
          lastname: "Doe",
          role: "user",
        }
      })

      pendingUser = await prisma.user.create({
        data: {
          email: "john@doe2.com",
          password: hashedPassword,
          firstname: "John",
          lastname: "Doe",
          role: "user",
          status: "pending"
        }
      })

      rejectedUser = await prisma.user.create({
        data: {
          email: "john@doe3.com",
          password: hashedPassword,
          firstname: "John",
          lastname: "Doe",
          role: "user",
          status: "rejected"
        }
      })
      
    })

    afterAll(async () => {

      await prisma.user.delete({
        where: {
          id: user!.id
        }
      })

      await prisma.user.delete({
        where: {
          id: pendingUser!.id
        }
      })

      await prisma.user.delete({
        where: {
          id: rejectedUser!.id
        }
      })

    })

    it("should fail if email missing", () => {
    
      supertest(app)
      .post("/api/login")
      .send({
        password: "password"
      })
      .expect(400)
      .expect({message: "Identifiants manquants"})

    })

    it("should fail if password missing", () => {

      supertest(app)
      .post("/api/login")
      .send({
        email: "test@test.com"
      })
      expect(400)
      expect({message: "Identifiants manquants"})

    })

    it("should fail if user unknown", () => {
        
        supertest(app)
        .post("/api/login")
        .send({
          email: "abc@abc.com",
          password: "password"
        })
        .expect(401)

    })

    it("should fail if user pending", () => {

      supertest(app)
      .post("/api/login")
      .send({
        email: pendingUser!.email,
        password: "password"
      })
      .expect(401)
      .expect({message: "Votre compte est en attente de validation"})

    })
      
    it("should fail if user rejected", () => {

      supertest(app)
      .post("/api/login")
      .send({
        email: rejectedUser!.email,
        password: "password"
      })
      .expect(401)
      .expect({message: "Vous avez été banni"})

    })

    it("should fail if password incorrect", () => {

      supertest(app)
      .post("/api/login")
      .send({
        email: user!.email,
        password: "wrongpassword"
      })
      .expect(401)
      .expect({message: "Identifiants incorrects"})

    })

    it("should connect user if credentials are valid", () => {

      supertest(app)
      .post("/api/login")
      .send({
        email: user!.email,
        password: "password"
      })
      .expect(200)
      .expect((response) => {
        expect(response.body.message).toBe("Connecté")
        expect(response.body.data.user).toBeDefined()
      })

    })

  })

  describe('Given user try to logout', () => {

    it("should disconnect user", () => {

      supertest(app)
      .get("/api/logout")
      .expect(200)
      .expect({message: "Déconnecté"})

    })

  })

  describe("Given user try to check token", () => {

    let token;
    let user;

    beforeAll(async () => {

      user = await prisma.user.create({
        data: {
          email: "test@checktoken.com",
          password: "password",
          firstname: "John",
          lastname: "Doe",
          role: "user",
        }
      })

      token = jwt.sign(
        {
          sub: user!.id,
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7 // 1 week
        },
        jwtSecretKey
      )

    })

    afterAll(async () => {

      await prisma.user.delete({
        where: {
          id: user!.id
        }
      })

    })

    it("should fail if token missing", () => {

      supertest(app)
      .post("/api/checkToken")
      .send({})
      .expect(401)
      .expect({message: "Token manquant"})

    })

    it("should fail if token invalid", () => {

      supertest(app)
      .post("/api/checkToken")
      .send({
        token: "invalid"  
      })
      .expect(401)
      .expect({message: "Token invalide"})

    })

    it("should fail if user unknown", () => {

      const unknownToken = jwt.sign(
        {
          sub: "unknown",
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7 // 1 week
        },
        jwtSecretKey
      )

      supertest(app)
      .post("/api/checkToken")
      .send({
        token: unknownToken
      })
      .expect(401)
      .expect({message: "Token invalide"})

    })

    it("should succeed if token valid", () => {

      supertest(app)
      .post("/api/checkToken")
      .send({
        token: token!
      })
      .expect(200)
      .expect((response) => {
        expect(response.body.message).toBe("Token valide")
      })

    })

  })

})