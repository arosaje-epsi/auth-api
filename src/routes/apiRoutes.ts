const router = require("express").Router();
import { adminLogin, checkJwtToken, userLogin, userLogout } from "../controllers/authController";

router.post('/login', userLogin)
router.post('/login/admin', adminLogin)
router.get('/logout', userLogout)
router.post('/checkToken', checkJwtToken)

module.exports = router;