const jwt = require("jsonwebtoken");

import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});

const jwtSecretKey = process.env.JWT_SECRET_KEY;

export const generateJwtToken = (userId:string) => {
  return jwt.sign(
    {
      sub: userId,
      exp: Date.now() + 1000 * 60 * 60 * 24 * 7 // 1 week
    },
    jwtSecretKey
  )
}

export const verifyJwtToken = (token:string) => {
  return jwt.verify(token, jwtSecretKey)
}