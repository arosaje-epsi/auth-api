import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import { generateJwtToken, verifyJwtToken } from "../utils/jwt";
const prisma = new PrismaClient();

export const userLogin = async (req:any, res:any, next:any) => {
  const { email, password } = req.body;

  // On vérifie si les identifiants sont bien envoyés
  if (!email || !password) {
    return res.status(400).json({message: "Identifiants manquants"})
  }

  // On tente la connexion
  try {
    const user = await prisma.user.findUnique({
      where: { email: email }
    })

    // Utilisateur inconnu
    if (!user) {
      return res.status(401).json({message: "Identifiants incorrects"})
    }

    // On vérifie que l'utilisateur soit validé
    if(user.status === "pending") {
      return res.status(401).json({message: "Votre compte est en attente de validation"})
    }

    if(user.status === "rejected") {
      return res.status(401).json({message: "Vous avez été banni"})
    }

    // On vérifie le mot de passe
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      return res.status(401).json({message: "Identifiants incorrects"})
    }

    // On génère le token
    const jwtToken = generateJwtToken(user.id)

    // On génère l'user renvoyé
    const returnedUser = {
      id: user.id,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      role: user.role,
      avatar: user.avatar,
      createdAt: user.createdAt,
    }
    
    return res.status(200)
      .cookie("jwt", jwtToken, {httpOnly:true,expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 7)})
      .json({message: "Connecté", data: {user:{...returnedUser}}})
  
    } catch (error) {
    return res.status(500).json({message: "Erreur serveur"})
  }
}

export const adminLogin = async (req:any, res:any, next:any) => {

  const { email, password } = req.body;

  // On vérifie si les identifiants sont bien envoyés
  if (!email || !password) {
    return res.status(400).json({message: "Identifiants manquants"})
  }

  // On tente la connexion
  try {
    const user = await prisma.user.findUnique({
      where: { email: email }
    })

    // Utilisateur inconnu
    if (!user) {
      return res.status(401).json({message: "Identifiants incorrects"})
    }

    // On vérifie que l'utilisateur soit validé
    if(user.status === "pending") {
      return res.status(401).json({message: "Votre compte est en attente de validation"})
    }

    if(user.status === "rejected") {
      return res.status(401).json({message: "Vous avez été banni"})
    }

    // On vérifie le mot de passe
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      return res.status(401).json({message: "Identifiants incorrects"})
    }

    // On vérifie que l'utilisateur soit admin
    if(user.role !== "admin") {
      return res.status(401).json({message: "Vous n'êtes pas administrateur"})
    }

    // On génère le token
    const jwtToken = generateJwtToken(user.id)

    // On génère l'user renvoyé
    const returnedUser = {
      id: user.id,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      role: user.role,
      avatar: user.avatar,
      createdAt: user.createdAt,
    }
    
    return res.status(200)
      .cookie("jwt", jwtToken, {httpOnly:true,expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 7)})
      .json({message: "Connecté", data: {user:{...returnedUser}}})
  
    } catch (error) {
    return res.status(500).json({message: "Erreur serveur"})
  }


}

export const userLogout = async (req:any, res:any, next:any) => {
  return res.status(200)
    .clearCookie("jwt")
    .json({message: "Déconnecté"})
}

export const checkJwtToken = async (req:any, res:any, next:any) => {
  const jwtToken = req.body.jwt

  // Paramètre manquant
  if (!jwtToken) {
    return res.status(401).json({message: "Token manquant"})
  }

  try {
    
    const decodedToken = verifyJwtToken(jwtToken)
    const userId = decodedToken.sub

    // On vérifie que l'user existe
    const user = await prisma.user.findUnique({
      where: { id: userId }
    })

    if (!user) {
      return res.status(401).json({message: "Token invalide"})
    }

    if(user.status === "pending" || user.status === "rejected") {
      return res.status(401).json({message: "Token invalide"})
    }

    // On génère l'user renvoyé
    const returnedUser = {
      id: user.id,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      role: user.role,
      avatar: user.avatar,
      createdAt: user.createdAt,
    }

    return res.status(200).json({message: "Token valide", data: {user:{...returnedUser}}})

  } catch (error) {
    return res.status(401).json({message: "Token invalide"})
  }
}